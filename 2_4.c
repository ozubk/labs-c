#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#define A 15


int main()
{
	int i,j;
	char x;
	char arr[A];
	srand(time(0));
	for(i=0;i<A-1;i++)
	{
		arr[i]=(rand()%2?rand()%('z'-'a'+1)+'a':rand()%('9'-'0'+1)+'0');
	}
	arr[A-1]=0;          
	printf("%s\n",arr); 
	for(i=0;i<A-1;i++)
	{
		for(j=0;j<A-2;j++)
		{
				if(isalpha(arr[j]))
				{
					x=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=x;
				}
		}
	}
	printf("%s\n",arr);

	return 0;
}

