#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10
#define M 9

int main()
{
	char arr[N][M];
	int i,j;
	srand(time(0));

	for(i=0;i<N;i++)
	{
		for(j=0;j<M-1;j++)
		{
			switch(rand()%3)
			{
			case 0: 
				arr[i][j]=rand()%('Z'-'A'+1)+'A';
				break;
			case 1:
				arr[i][j]=rand()%('z'-'a'+1)+'a';
				break;
			case 2:
				arr[i][j]=rand()%('9'-'0'+1)+'0';
				break;
			}
		}
		arr[i][M-1]=0;
		printf("%s\n",arr[i]);
	}

	return 0;
}
			




