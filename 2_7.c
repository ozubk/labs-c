/*
#include <stdio.h>
#include <string.h>

#define A 256

int main()
{
	char arr[A];
	int i,j,count=0;
	char a;
	printf("Enter string\n");
	fgets(arr,A,stdin);
	arr[j=strlen(arr)-1]=0;
	for(a=-128;a<127;a++)  
	{
		for(i=0;i<j;i++)
		{
			
			if(a==arr[i])
				count++;
		}
		if(count!=0)
		{
			printf("%c %d\n",a,count);
			count=0;
		}
		
	}
	return 0;
}

*/

#include <stdio.h>
#include <string.h>

#define A 500

int main()
{
	unsigned char arr[A];
	int i=0;
	int counts[256]={0};
	puts("Enter string");
	fgets(arr,A,stdin);
	arr[(strlen(arr)-1)]=0;
	while(arr[i])
		counts[arr[i++]]++;
	for(i=0;i<256;i++)
		if(counts[i]!=0)
			printf("%c %d\n",i,counts[i]);

	return 0;
}